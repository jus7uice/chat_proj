<?php

Route::get(ADMIN_PATH,'Auth\AuthAdminCtr@index')->middleware('cek.auth','cek.ipwhitelist');
Route::get(ADMIN_PATH.'logout','Auth\AuthAdminCtr@logout');

Route::group(['middleware' => 'web'], function () {
	// Route::post(ADMIN_PATH.'authadmin','Auth\AuthAdminCtr@postAuth');
	Route::post(ADMIN_PATH.'authadmin','Auth\AuthAdminCtr@postAuthWithThrottle'); // With throttle
});

Route::group(['prefix'=>ADMIN_PATH, 'namespace'=> 'Backend\\','middleware' => ['auth.admin','cek.ipwhitelist']], function () {
	
	Route::get('err/ip.blocked',function(){return view('special.err_ip_block');});
	Route::get('err/restrical.access',function(){return view('special.err_restrical_access');})->middleware('auth.admin');
	
	Route::get('dashboard','ApplicationCtr@index');
	
	Route::get('setting.general','SettingCtr@index');
	Route::post('setting.general.post','SettingCtr@postGeneral');	

	/* Aktifkan Jika ingin menggunakan IP Whitelist */
	Route::get('setting.ipwhitelist','IpWhitelistCtr@index');
	Route::get('setting.ipwhitelist.data','IpWhitelistCtr@getData');
	Route::get('setting.ipwhitelist.create','IpWhitelistCtr@getCreate');
	Route::post('setting.ipwhitelist.create','IpWhitelistCtr@postCreate');
	Route::post('setting.ipwhitelist.delete','IpWhitelistCtr@postDelete');
	
	Route::get('setting.theme','ThemeCtr@index');
	Route::post('setting.theme','ThemeCtr@postData');
	
	Route::post('admin.profile.post','AdminCtr@postMe');
	Route::post('admin.profile.chgpass','AdminCtr@postChangePassword');
	Route::get('admin.profile','AdminCtr@getMe');
	
	Route::get('admin.user','AdminCtr@getUsers');
	Route::get('admin.user.data','AdminCtr@getUsersData');
	Route::get('admin.user.create','AdminCtr@createUser');
	Route::post('admin.user.create','AdminCtr@postCreateUser');
	Route::get('admin.user.edit','AdminCtr@editUser');
	Route::post('admin.user.edit','AdminCtr@postEditUser');
	Route::post('admin.user.delete','AdminCtr@postDeleteUsers');
	
	Route::get('admin.group','AdminGroupCtr@index');
	Route::get('admin.group.data','AdminGroupCtr@getGroupsData');
	Route::get('admin.group.create','AdminGroupCtr@createGroup');
	Route::post('admin.group.create','AdminGroupCtr@postCreateGroup');
	Route::get('admin.group.edit','AdminGroupCtr@editGroup');
	Route::post('admin.group.edit','AdminGroupCtr@postEditGroup');
	Route::post('admin.group.delete','AdminGroupCtr@postDeleteGroups');
	
	Route::get('admin.log','AdminLogCtr@index');
	Route::get('admin.log.data','AdminLogCtr@getData');
	
	Route::get('channel','ChannelCtr@index');
	Route::get('channel.data','ChannelCtr@getData');
	Route::get('channel.create','ChannelCtr@getCreate');
	Route::post('channel.create','ChannelCtr@postCreate');
	Route::get('channel.edit','ChannelCtr@getEdit');
	Route::post('channel.edit','ChannelCtr@postEdit');
	Route::post('channel.delete','ChannelCtr@postDelete');
	
	Route::get('restrical.username','RestricalUsernameCtr@index');
	Route::get('restrical.username.data','RestricalUsernameCtr@getData');
	Route::get('restrical.username.create','RestricalUsernameCtr@getCreate');
	Route::post('restrical.username.create','RestricalUsernameCtr@postCreate');
	Route::get('restrical.username.edit','RestricalUsernameCtr@getEdit');
	Route::post('restrical.username.edit','RestricalUsernameCtr@postEdit');
	Route::post('restrical.username.delete','RestricalUsernameCtr@postDelete');
	
	Route::get('tag','TagCtr@index');
	Route::get('tag.data','TagCtr@getData');
	Route::get('tag.json','TagCtr@getJson');
	Route::get('tag.create','TagCtr@getCreate');
	Route::post('tag.create','TagCtr@postCreate');
	Route::get('tag.edit','TagCtr@getEdit');
	Route::post('tag.edit','TagCtr@postEdit');
	Route::post('tag.delete','TagCtr@postDelete');
	
	Route::get('user','UserCtr@index');
	Route::get('user.data','UserCtr@getData');
	Route::get('user.create','UserCtr@getCreate');
	Route::post('user.create','UserCtr@postCreate');
	Route::get('user.edit','UserCtr@getEdit');
	Route::post('user.edit','UserCtr@postEdit');
	Route::post('user.delete','UserCtr@postDelete');
	
	Route::get('post','PostCtr@index');
	Route::get('post.data','PostCtr@getData');
	Route::get('post.create','PostCtr@getCreate');
	Route::post('post.create','PostCtr@postCreate');
	Route::get('post.edit','PostCtr@getEdit');
	Route::post('post.edit','PostCtr@postEdit');
	Route::post('post.delete','PostCtr@postDelete');
	
	Route::post('media.upload','MediaCtr@postUpload');
	Route::get('media.json','MediaCtr@getData');	
	Route::get('media','MediaManagerCtr@index');
	Route::get('media.data','MediaManagerCtr@getData');
	Route::get('media.create','MediaManagerCtr@getCreate');
	Route::post('media.create','MediaManagerCtr@postCreate');
	Route::get('media.edit','MediaManagerCtr@getEdit');
	Route::post('media.edit','MediaManagerCtr@postEdit');
	Route::post('media.delete','MediaManagerCtr@postDelete');
	Route::get('media.delete','MediaManagerCtr@getDelete');
	
	Route::get('slideshow','SlideshowCtr@index');
	Route::get('slideshow.data','SlideshowCtr@getData');
	Route::get('slideshow.create','SlideshowCtr@getCreate');
	Route::post('slideshow.create','SlideshowCtr@postCreate');
	Route::get('slideshow.edit','SlideshowCtr@getEdit');
	Route::post('slideshow.edit','SlideshowCtr@postEdit');
	Route::post('slideshow.delete','SlideshowCtr@postDelete');
	
	
});


?>