<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');
// });


Route::get('/', 'AppCtr@index');
Route::get('/create', function () {
    return view('create');
});
Route::post('/create', 'AppCtr@index');
Route::get('/post_comment', function () {
    return view('post_comment');
});
Route::get('/view', function () {
    return view('view');
});
Route::get('/embed', function () {
    return view('embed');
});

/* Backend Route */
require '_backend.php';
