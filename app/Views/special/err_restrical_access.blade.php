@extends('layouts.backend')

@section('content')
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>

    <!-- Main content -->
    <section class="contentXXX">

      <div class="error-page">
        <h2 class="headline text-red">403</h2>

        <div class="error-content">
          <p>&nbsp;</p>
		  <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
          <p>
		  {{trans('message.restrical_access')}}
          </p>
          <p>
		  {{trans('message.please_contact_admnistrator')}}
          </p>
         
        </div>
      </div>
      <!-- /.error-page -->

    </section>
    <!-- /.content -->


@endsection