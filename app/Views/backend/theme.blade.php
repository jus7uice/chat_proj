@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- Theme
@endsection

{{-- Page title --}}
@section('pagetitle')
	Setting <small>Theme</small>
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        <div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'setting.theme')}}" method="post">
				<div class="box-header with-border">
				  <h3 class="box-title">Theme</h3>
				</div>
				<div class="box-body">
				
					<div class="input-group">
						<span class="input-group-addon">{{trans('general.label.skin')}}</span>			
						{{Form::select('skin', $skinLists, $skin,['class'=>'form-control'])}}
					</div>
					
					<!--
					<div class="input-group">
						<span class="input-group-addon">{{trans('general.label.theme')}}</span>
						<span class="form-control">
						<label class="radio-inline">{{Form::radio('theme', 'THEME1', ($theme=='THEME1')?true:false)}} Top Navigation Layout (Default)</label>
						<label class="radio-inline">{{Form::radio('theme', 'THEME2', ($theme=='THEME2')?true:false)}} Left Side Navigation </label>
						</span>
					</div>
					-->
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-success">{{trans('general.button.save')}}</button>
				</div>
				{{csrf_field()}}
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection