<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Throttle extends Model
{
    protected $table = "throttle";
    protected $primaryKey = "session";
	public $timestamps = false;
	protected $fillable = [
        'session', 'ip', 'try', 'start_time', 'end_time'
    ];
}
