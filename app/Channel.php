<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{

	protected $guarded = [];
	
	//each channel might have one parent
	public function parent() {
		return $this->belongsTo(static::class, 'parent_id');
	}

	//each channel might have multiple children
	public function children() {
		return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
	}
  
}
