<?php

return [

    'save' => array(
        'error'   => 'There was a problem while trying to save the data, please try again.',
        'success' => 'Data save successfully.',
    ),
	'delete' => array(
        'error'   => 'There was a problem while trying to delete the data, please try again.',
        'success' => 'Data deleted successfully.',
    ),
	'update' => array(
        'error'   => 'There was a problem while trying to update the data, please try again.',
        'success' => 'Update update successfully.',
    ),
	'current_password_wrong' => 'Your current password is wrong. Please type your current password',
	'new_password_match_wrong' => 'Your new password confirmation are not the same, please check again.',
	'ip_blocked' => 'We are so sorry, your IP Address disallowed to access this domain',
	'restrical_access' => 'You don\'t have permission to access this page',
	'access_denied' => 'Access denied.',
    'logout_success' => 'Logout successfully..',
    'please_contact_admnistrator' => 'Please contact your Administrator for more informations.',
    'comment_change_password' => 'Please leave it blank. Fill only if you want to change current user password.',
    'date_range_wrong' => 'Date range is wrong. Please correct it.',
    'login_other_device_detected' => 'Other device sign in detected.',

];
